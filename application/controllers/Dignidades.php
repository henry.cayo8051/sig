<?php
class Dignidades extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    //cargar todos los modelos que necesitemos
    $this->load->model('Dignidad');
  }
  //Funcion que renderiza la vista index
  public function index()
  {
    $data['dignidades'] = $this->Dignidad->obtenerTodos();
    //print_r($productos);
    $this->load->view('header');
    $this->load->view('dignidades/index', $data);
    $this->load->view('footer');
  }
  public function nuevo()
  {
    $this->load->view('header');
    $this->load->view('dignidades/nuevo');
    $this->load->view('footer');
  }
  public function reporte()
  {
    $this->load->view('header');
    $this->load->view('dignidades/reporte');
    $this->load->view('footer');
  }

  public function naci()
  {
    $data["tarea"]=$this->Dignidad->obtenerNaci();
    $this->load->view('header');
    $this->load->view('dignidades/naci', $data);
    $this->load->view('footer');
  }
  public function provi()
  {
    $data["tarea"]=$this->Dignidad->obtenerProvi();
    $this->load->view('header');
    $this->load->view('dignidades/provi', $data);
    $this->load->view('footer');
  }
  public function todo()
  {
    $data["tarea"]=$this->Dignidad->obtenerGeneral();
    $this->load->view('header');
    $this->load->view('dignidades/todo', $data);
    $this->load->view('footer');
  }
  public function guardar()
  {
    $datosNuevoDignidad = array(
      "dignidad_deber" => $this->input->post('dignidad_deber'),
      "apellido_deber" => $this->input->post('apellido_deber'),
      "nombre_deber" => $this->input->post('nombre_deber'),
      "movimiento_deber" => $this->input->post('movimiento_deber'),
      "correo_deber" => $this->input->post('correo_deber'),
      "telefono_deber" => $this->input->post('telefono_deber'),
      "latitud_deber" => $this->input->post('latitud_deber'),
      "longitud_deber" => $this->input->post('longitud_deber'),
    );


    //imprime los datos del array que creamos
    print_r($datosNuevoDignidad);
    if ($this->Dignidad->insertar($datosNuevoDignidad)) {
      redirect('dignidades/index');
    } else {
      echo "<h1>  Error al insertar datos</h1>";
    }
  }
  // funcion para eliminara istructores
  public function eliminar($id_prod)
  {
    //echo $id_prod;
    if ($this->Dignidad->borrar($id_prod)) { //invocando al modelo
      redirect('dignidades/index');
    } else {
      echo "ERROR AL ELIMINAR :(";
    }
  }

  public function presi()
  {
    $data["tarea"]=$this->Dignidad->obtenerTodo();
    $this->load->view('header');
    $this->load->view('dignidades/presi', $data);
    $this->load->view('footer');
  }

}//Ciere de la clase
?>
