
<h1 class="text-center" > REPORTES </h1>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="">
</head>
<body class="text-center">
  <div class="container">
     <div class="">
       <a href="<?php echo site_url(); ?>/dignidades/presi" class="btn btn-primary"> R. PRESIDENTE</a>
     </div>
     <br>
     <div class="">
       <a href="<?php echo site_url(); ?>/dignidades/naci" class="btn btn-primary"> A. NACIONAL</a>
     </div>
     <br>
     <div class="">
       <a href="<?php echo site_url(); ?>/dignidades/provi" class="btn btn-primary"> A. PROVINCIAL</a>
     </div>
     <br>
     <div class="">
       <a href="<?php echo site_url(); ?>/dignidades/todo" class="btn btn-primary"> R. GENERAL</a>
     </div>
  </div>
  <br>
</body>
</html>
