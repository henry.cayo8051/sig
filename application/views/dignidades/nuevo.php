<h1 class="text-center"> NUEVO DE CANDIDATOS </h1>
<form class="" action="<?php echo site_url(); ?>/dignidades/guardar" method="post">
  <div class="container">
    <div class="row">
        <div class="col-md-4">
            <label for="">DIGNIDADES:</label>
            <br>
            <select   class="form-control" type="text" name="dignidad_deber" class="form-control input-sm" required id="dignidad_deber">
                <option selected placeholder="selecione la dignidad">SELECIONE</option>
                <option value="PRESIDENTE">PRESIDENTE</option>
                <option value="ASAMBLEISTA NACIONA">ASAMBLEISTA NACIONAL</option>
                <option value="ASAMBLESITA PROVINCIAL">ASAMBLEISTA PROVINCIAL</option>
            </select>
        </div>

        <div class="col-md-4">
            <label for="">APELLIDO DEL CAMDIDATO:</label>
            <br>
            <input type="text" placeholder="Ingrese el apellido" class="form-control" name="apellido_deber" value="" id="apellivdo_deber">
        </div>
        <div class="col-md-4">
            <label for="">NOMBRE DEL CANDIDATO:</label>
            <br>
            <input type="text" placeholder="Ingrese el Nombre" class="form-control" name="nombre_deber" value="" id="nombre_deber">
        </div>
        <br>
        <div class="col-md-4">
            <label for="">MOVIMINETO DEL CANDIDATO:</label>
            <br>
            <input type="text" placeholder="Ingrese el Movimiento" class="form-control" name="movimiento_deber" value="" id="movimiento_deber">
        </div>
        <br>
        <div class="col-md-4">
            <label for="">CORREO DEL CANDIDATO:</label>
            <br>
            <input type="text" placeholder="Ingrese el Correo" class="form-control" name="correo_deber" value="" id="correo_deber">
        </div>
        <br>
        <div class="col-md-4">
            <label for="">TELEFONO DEL CANDIDATO:</label>
            <br>
            <input type="text" placeholder="Ingrese el Telefono" class="form-control" name="telefono_deber" value="" id="telefono_deber">
        </div>
        <br>

        <div class="col-md-4">
            <label for="">LATITUD DEL CANDIDATO:</label>
            <br>
            <input type="text" placeholder="Ingrese la latitud" class="form-control" readonly name="latitud_deber" value="" id="latitud_deber">
        </div>
        <br>
        <div class="col-md-4">
            <label for="">LONGITUD DEL CANDIDATO:</label>
            <br>
            <input type="text" placeholder="Ingrese la longitud" class="form-control" readonly name="longitud_deber" value="" id="longitud_deber">
        </div>
    </div>
    </div>
    <br>
    <br>
   <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="mapaUbicacion" style="height:300px; with:100%; border:2px solid black;"></div>
      </div>
    </div>
    </div>
<script type="text/javascript">
     function initMap(){
        var centro = new google.maps.LatLng(-2.1732687811038756, -79.43839246288748);
        var mapa1=new google.maps.Map(
          document.getElementById('mapaUbicacion'),
          {
            center:centro,
            zoom:7,
            mapTypeId:google.maps.MapTypeId.ROADMAP
          }
        );
        var marcador=new google.maps.Marker({
          position:centro,
          map:mapa1,
          title:"Selecione la direccion",
          //icon:"ruta"
          draggable:true
        });
        google.maps.event.addListener(marcador, 'dragend', function (){
        //  alert("se termino el Drag");
        document.getElementById('latitud_deber').value=
        this.getPosition().lat();
        document.getElementById('longitud_deber').value=
        this.getPosition().lng();
        });

     }//cierre de la fincion

</script>

    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-primary">
                Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/dignidades/index" class="btn btn-danger">
                Cancelar
            </a>
        </div>
    </div>
    <br>

</form>
