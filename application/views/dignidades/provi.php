<h1 class="text-center"> REPORTE DE LOS ASABLEISTA PROVINCIALES </h1>
<div class="row">
    <div class="col-md-8">
    </div>
    <div class="col-md-4">
        <a href="<?php echo site_url('dignidades/reporte') ?>" class="btn btn-primary"> <i> REPORTE </i></a>
    </div>
</div>
<br>
<div class="row">
  <div class="col md-12">
    <div id="mapaLugares" style="height:500px; width:100%; border:2px solid black;">
    </div>
  </div>
</div>
<br>
<script type="text/javascript">
		function initMap() {
			var centro = new google.maps.LatLng(-0.9214680500257709, -78.62050563183242);
			var mapaProvi = new google.maps.Map(
				document.getElementById('mapaLugares'), {
					center: centro,
					zoom: 3,
					mapTypeId: google.maps.MapTypeId.HYBRID
				}
			);
			<?php if ($tarea) : ?>
				<?php foreach ($tarea as $lugarTemporal) : ?>
					var coodenadaTemporal = new google.maps.LatLng(<?php echo $lugarTemporal->latitud_deber; ?>, <?php echo $lugarTemporal->longitud_deber; ?>);
					var marcador = new google.maps.Marker({
						position: coodenadaTemporal,
						title: "<?php echo $lugarTemporal->telefono_deber; ?>",
            icon:"<?php echo base_url(); ?>/assets/images/provii.png",
						map: mapaProvi
					});
				<?php endforeach; ?>
			<?php endif; ?>

		}
	</script>
