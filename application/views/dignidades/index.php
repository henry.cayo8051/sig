<h1 class="text-center"> LISTADO DE LOS CANDIDATOS </h1>
<div class="row">
    <div class="col-md-8">
    </div>
    <div class="col-md-4">
        <a href="<?php echo site_url('dignidades/nuevo') ?>" class="btn btn-primary"> <i class="glyphicon glyphicon-plus"> NUEVO CANDIDADTO </i></a>

    </div>
</div>
<br>
<?php if ($dignidades) : ?>
    <table class="table table-striped
    table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>DIGNIDAD</th>
                <th>APELLIDO DEL CANDIDATO</th>
                <th>NOMBRE DEL CANDIDATO</th>
                <th>MOVIMIENTO DEL CANDIDATO</th>
                <th>CORREO DEL CANDIDATO</th>
                <th>TELEFONO DEL CANDIDATO</th>
                <th>LATITUD DEL CANDIDATO</th>
                <th>LONGITUD DEL CANDIDATO</th>
                <th>ACCIONES</th>

            </tr>
        </thead>
        <tbody>
            <?php foreach ($dignidades
                as $filaTemporal) : ?>
                <tr>
                    <td>
                        <?php echo
                        $filaTemporal->id_deber; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->dignidad_deber; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->apellido_deber; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->nombre_deber; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->movimiento_deber; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->correo_deber; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->telefono_deber; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->latitud_deber; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->longitud_deber; ?>
                    </td>
                    <td class="text-center">
                        <a href="#" title="Editar Dignidad" style="color:green;">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        <a href="<?php echo site_url(); ?>/dignidades/eliminar/<?php echo $filaTemporal->id_deber; ?>" title="Eliminar Dignidad" onclick="return confirm('ESTA SEGURO DE ELIMINAR ESTE CANDIDATO ?')" style="color:red;">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>

    </table>
<?php else : ?>
    <h1>NO HAY DATOS DE LOS PRODUTOS</h1>
<?php endif ?>
