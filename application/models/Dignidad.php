<?php
class Dignidad extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  function insertar($datos)
  {
    return $this->db->insert(
      "tarea",
      $datos
    );
  }

  function obtenerTodos()
  {
    $listadoDignidades =
      $this->db->get("tarea");
    if ($listadoDignidades->num_rows() > 0) {
      return $listadoDignidades->result();
    } else {
      return false;
    }
  }
  public function borrar($id_deber)
  {
    //borrar proveedor
    $this->db->where("id_deber", $id_deber);
    if ($this->db->delete('tarea')) {
      return true;
    } else {
      return false;
    }
  }
  function obtenerTodo  (){
    $listadoDignidades=$this->db->get("tarea");
    if ($listadoDignidades->num_rows() >0) {
      return $listadoDignidades->result();
    }
    return false;
  }
  function obtenerNaci(){
    $listadoNacionales=$this->db->get("tarea");
    if ($listadoNacionales->num_rows() >0) {
      return $listadoNacionales->result();
    }
    return false;
  }
  function obtenerProvi(){
    $listadoProvinciales=$this->db->get("tarea");
    if ($listadoProvinciales->num_rows() >0) {
      return $listadoProvinciales->result();
    }
    return false;
  }
  function obtenerGeneral(){
    $listadoGeneral=$this->db->get("tarea");
    if ($listadoGeneral->num_rows() >0) {
      return $listadoGeneral->result();
    }
    return false;
  }

}//Cierre de la clase
